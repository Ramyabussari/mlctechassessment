package StepDefs;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import net.thucydides.core.steps.ScenarioSteps;
import pages.MLCLifeViewPage;

public class MLCLifeViewPageSteps extends ScenarioSteps {

    MLCLifeViewPage MLCLifeViewPage;

    @And("at lifeview page - user verifies breadcrumbs containing {string}")
    public void atLifeviewPageUserVerifiesBreadcrumbsContaining(String text) {
        MLCLifeViewPage.verifyText(text);
    }

    @And("at lifeview page - user clicks on {string}")
    public void atLifeviewPageUserClicksOn(String option) {
        MLCLifeViewPage.clickOnOption(option);
    }

    @And("lifeview page is loaded")
    public void lifeviewPageIsLoaded() {
        MLCLifeViewPage.pageIsLoaded();
    }

    @And("at lifeview page - user enters below text in various fields")
    public void atLifeviewPageUserEntersBelowTextInVariousFields(DataTable dataTable) {
        MLCLifeViewPage.fillRequestDemoForm( dataTable);
    }
}
