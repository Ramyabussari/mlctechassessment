package StepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import net.thucydides.core.steps.ScenarioSteps;
import pages.MLCHomePage;
import pages.PageHelper;


public class MLCHomePageSteps extends ScenarioSteps {
    MLCHomePage MLCHomePage;

    PageHelper pageHelper;

    @Given("user launches the mlcinsurance website")
    public void userLaunchesTheMlcinsuranceWebsite() {
        MLCHomePage.launchHomePage();

    }

    @And("home page is loaded")
    public void homePageIsLoaded() {
        MLCHomePage.pageIsLoaded();
    }

    @And("at home page - user clicks on {string}")
    public void atHomePageUserClicksOn(String optionType) {
        MLCHomePage.clickOnOption(optionType);
    }

    @And("at home page - user search in SearchBox for {string}")
    public void atHomePageUserSearchInFor(String text) {
        MLCHomePage.enterSearchText(text);
    }

    @And("at home page - user selects {string} from the list")
    public void atHomePageUserSelectsFromTheList(String option) {
        MLCHomePage.selectFromList(option);
    }

    @And("user verifies home page is displayed")
    public void userVerifiesHomePageIsDisplayed() {
        MLCHomePage.verifyHomePageisDisplayed();
    }

    @And("user maximizes the window")
    public void userMaximizesTheWindow() {
        pageHelper.maximizeWindow();
    }
}
