package StepDefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import net.thucydides.core.steps.ScenarioSteps;
import pages.ATOCalculatorPage;


public class ATOCalculatorPageSteps extends ScenarioSteps {
    ATOCalculatorPage atoCalculatorPage;

    @Given("user launches the ATO website")
    public void userLaunchesTheMlcinsuranceWebsite() {
        atoCalculatorPage.launchHomePage();
    }


    @And("at calculator page - user selects the {string}")
    public void atCalculatorPageUserSelectsThe(String option) {
        atoCalculatorPage.selectOption(option);
    }

    @And("at calculator page - user provides the {string}")
    public void atCalculatorPageUserProvidesThe(String taxableAmount) {
        atoCalculatorPage.provideAmount(taxableAmount);
    }

    @And("at calculator page - user clicks on {string}")
    public void atCalculatorPageUserClicksOn(String option) {
        atoCalculatorPage.clickOnOption(option);
    }

    @And("at calculator page - user selects radiobutton {string}")
    public void atCalculatorPageUserSelectsRadiobutton(String index) {
        atoCalculatorPage.selectRadioButton(index);
    }

    @And("calculator page is loaded")
    public void calculatorPageIsLoaded() {
        atoCalculatorPage.pageIsLoaded();
    }

    @And("at calculator page - user verifies the {string}")
    public void atCalculatorPageUserVerifiesThe(String expectedTax) {
        atoCalculatorPage.verifyTaxAmount(expectedTax);

    }
}
