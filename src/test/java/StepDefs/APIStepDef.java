package StepDefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.steps.ScenarioSteps;
import pages.APIValidator;

public class APIStepDef extends ScenarioSteps {
    APIValidator apiValidator;


    @Given("user retrieves the shipping cost for {string} with {string} and {string}")
    public void userRetrievesTheShippingCostForWithAnd(String countryCode,String weight,String serviceCode) {
        apiValidator.getRequestDetails(countryCode,weight,serviceCode);
    }

    @Given("user is able to access the api with status {int}")
    public void userIsAbleToAccessTheApi(int statuscode) {
        apiValidator.validateAPIAccess(statuscode);
    }

}
