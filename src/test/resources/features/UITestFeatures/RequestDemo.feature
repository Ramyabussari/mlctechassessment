@requestdemoform
Feature:Validate the user is able to fill up the LifeView Funds form

  @requestdemoform
  Scenario: Validate the user is able to fill up the LifeView Funds form
    Given user launches the mlcinsurance website
    And user verifies home page is displayed
    And user maximizes the window
    And home page is loaded
    And at home page - user clicks on "Search Icon"
    And at home page - user search in SearchBox for "LifeView"
    And at home page - user selects "LifeView" from the list
    And lifeview page is loaded
    And at lifeview page - user verifies breadcrumbs containing "Home Partnering with us Superannuation funds LifeView"
    And at lifeview page - user clicks on "Request a demo"
    And at lifeview page - user enters below text in various fields
      | Name  | Company | Email          | Phone      | Contact Date | Request Details |
      | Ramya | Test    | test@gmail.com | 0401456745 | 05/06/2021   | Test            |
