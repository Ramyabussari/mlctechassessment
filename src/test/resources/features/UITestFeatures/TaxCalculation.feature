@taxcalculation
Feature:Validate the tax is calculated correctly

  @taxcalculation
  Scenario Outline: Validate the tax is calculated correctly
    Given user launches the ATO website
    And user maximizes the window
    And calculator page is loaded
    And at calculator page - user selects the "<income year>"
    And at calculator page - user provides the "<taxable income>"
    And at calculator page - user selects radiobutton "<residency status number>"
    And at calculator page - user clicks on "Submit"
    And at calculator page - user verifies the "<estimated tax>"

    Examples:
      | income year | taxable income | residency status number | estimated tax |
      | 2019-20     | 100000         | 0                       | $24,497.00    |
      | 2018-19     | 110000         | 1                       | $36,650.00    |
      | 2017-18     | 110000         | 1                       | $36,785.00    |




