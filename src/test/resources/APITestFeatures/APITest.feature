@InternationalShippingCalculation
Feature: Using AUPOST API, calculate Shipping costs of international parcel delivery

  @InternationalShippingCalculation
  Scenario Outline: Calculate the shipping cost of parcels to different countries
    Given user is able to access the api with status 200
    Then user retrieves the shipping cost for "<Country>" with "<Weight>" and "<ServiceCode>"

    Examples:
      | Country | Weight | ServiceCode                  |
      | NZ      | 5      | INT_PARCEL_STD_OWN_PACKAGING |
      | USA     | 10     | INT_PARCEL_STD_OWN_PACKAGING |
      | IN      | 5      | INT_PARCEL_STD_OWN_PACKAGING |
