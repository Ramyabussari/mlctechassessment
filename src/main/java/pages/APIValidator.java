package pages;


import Utilities.ParameterProvider;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import net.serenitybdd.core.pages.PageObject;




public class APIValidator extends PageObject {
    private static String url=ParameterProvider.getParameter(ParameterProvider.ParameterName.URL, ParameterProvider.AppName.AUPOST);
    private static String apiKey = ParameterProvider.getParameter(ParameterProvider.ParameterName.AUTHORIZATION, ParameterProvider.AppName.AUPOST);

    public void getRequestDetails(String countryCode, String weight, String serviceCode) {

        RestAssured.baseURI = url;
        RestAssured.given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .header("AUTH-KEY", apiKey)
                .formParam("country_code", countryCode)
                .formParam("weight", weight).formParam("service_code", serviceCode)
                .when()
                .get(url + "/postage/parcel/international/service")
                .then()
                .statusCode(200)
                .extract().response().jsonPath().get("costs.cost.cost");

    }

    public void validateAPIAccess(int statuscode) {
        RestAssured.baseURI = url;
        RestAssured.given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .header("AUTH-KEY", apiKey)
                .get(url + "/postage/parcel/international/service")
                .then()
                .statusCode(200);
    }
}