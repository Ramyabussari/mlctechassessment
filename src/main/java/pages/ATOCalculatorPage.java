package pages;

import Utilities.ParameterProvider;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.HashMap;

public class ATOCalculatorPage extends PageObject {
    private static Logger LOGGER = LogManager.getLogger(ATOCalculatorPage.class);

    private static PageHelper pageHelper;

    private final HashMap<String, WebElementFacade> elementMap = new HashMap<>();

    @FindBy(xpath = "//h1[contains(text(),'Simple tax calculator')]")
    private WebElementFacade calculatorPage;

    @FindBy(xpath = "//select[@id='ddl-financialYear']")
    private WebElementFacade incomeYearDropDown;

    @FindBy(xpath = "//input[@id='texttaxIncomeAmt']")
    private WebElementFacade taxableIncomeInput;

    @FindBy(xpath = "//button[@id='bnav-n1-btn4']")
    private WebElementFacade submit;

    @FindBy(xpath = "//p[contains(text(),'The estimated tax on your taxable income is ')]")
    private WebElementFacade actualTaxAmount;


    public void pageIsLoaded() {
        elementMap.put("submit", submit);
    }

    private WebElement getElementIDforResidencyStatus(String index) {
        String elementID = "//span[@id='vrb-resident-span-<indexNum>']";
        return getDriver().findElement(By.xpath(elementID.replaceAll("<indexNum>", index)));
    }

    //launch home page
    public void launchHomePage() {
        String url= ParameterProvider.getParameter(ParameterProvider.ParameterName.URL, ParameterProvider.AppName.TAXCALUI);
        getDriver().get(url);
        if (calculatorPage.isVisible()) {
            System.out.println("Launched the page");
        } else {
            Assert.fail("Unable to launch  page");
        }
    }

//select an year from dropdown
    public void selectOption(String option) {
        try {
            pageHelper.selectDropDown(incomeYearDropDown, option);
        } catch (Exception e) {
            Assert.fail("Selecting dropdown failed" + e);
        }
    }

    public void provideAmount(String taxableAmount) {
        pageHelper.sendStringAsCharArray(taxableIncomeInput, taxableAmount);
    }

    public void clickOnOption(String option) {
        if(option.equalsIgnoreCase("Submit")){
            ((JavascriptExecutor) getDriver()).executeScript("scroll(0,1000)");
        }
        elementMap.get(option.toLowerCase()).click();
    }

    public void selectRadioButton(String index) {
        getElementIDforResidencyStatus(index).click();
    }

    public void verifyTaxAmount(String expectedTax) {
        String actualTax=actualTaxAmount.getText().replaceAll("The estimated tax on your taxable income is ","");
        Assert.assertEquals(expectedTax,actualTax);
    }
}