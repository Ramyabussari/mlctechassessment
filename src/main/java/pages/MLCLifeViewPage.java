package pages;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MLCLifeViewPage extends PageObject {
    private static Logger LOGGER = LogManager.getLogger(MLCLifeViewPage.class);

    private static PageHelper pageHelper;

    private final HashMap<String, WebElementFacade> elementMap = new HashMap<>();


    @FindBy(xpath = "*//ul[@itemprop='breadcrumb']")
    private WebElementFacade breadcrumb;

    @FindBy(xpath = "//span[contains(text(),'Request a demo')]")
    private WebElementFacade requestaDemo;

    @FindBy(xpath = "//input[@data-sc-field-name='Name']")
    private WebElementFacade nameField;

    @FindBy(xpath = "//input[@data-sc-field-name='Company']")
    private WebElementFacade companyField;

    @FindBy(xpath = "//input[@data-sc-field-name='Email']")
    private WebElementFacade emailField;

    @FindBy(xpath = "//input[@data-sc-field-name='Phone']")
    private WebElementFacade phoneField;

    @FindBy(xpath = "//input[@data-sc-field-name='Preferred contact date']")
    private WebElementFacade contactDateField;

    @FindBy(xpath = "//input[@value='AM']")
    private WebElementFacade preferredContactTimeField;

    @FindBy(xpath = "//textarea[@data-sc-field-name='Request details']")
    private WebElementFacade requestDetailsField;


    public void pageIsLoaded() {
        elementMap.put("request a demo", requestaDemo);
    }


    public void clickOnOption(String optionType) {
        try {
            elementMap.get(optionType.toLowerCase()).click();
        } catch (Exception e) {
            Assert.fail("Exception during button click " + e);
        }

    }


    public void verifyText(String expectedtext) {
        try {
            String actualText = breadcrumb.getText();
            Assert.assertEquals(expectedtext, actualText);

        } catch (Exception e) {
            Assert.fail("Actual and expected values are not matching" + e);
        }
    }

    public void fillRequestDemoForm(DataTable dataTable) {
        try {
            List<Map<String, String>> data = dataTable.asMaps();
            String name = data.get(0).get("Name");
            String company = data.get(0).get("Company");
            String email = data.get(0).get("Email");
            String phone = data.get(0).get("Phone");
            String contactDate = data.get(0).get("Contact Date");
            String details = data.get(0).get("Request Details");
            pageHelper.sendStringAsCharArray(nameField, name);
            pageHelper.sendStringAsCharArray(companyField, company);
            pageHelper.sendStringAsCharArray(emailField, email);
            pageHelper.sendStringAsCharArray(phoneField, phone);
            pageHelper.sendStringAsCharArray(contactDateField, contactDate);
            preferredContactTimeField.click();
            pageHelper.sendStringAsCharArray(requestDetailsField, details);

        } catch (Exception e) {
            Assert.fail("Exception in finding the option" + e);
        }
    }

}