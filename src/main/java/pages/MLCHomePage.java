package pages;

import Utilities.ParameterProvider;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.apache.log4j.LogManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;

public class MLCHomePage extends PageObject {
    private static Logger LOGGER = LogManager.getLogger(MLCHomePage.class);

    private static PageHelper pageHelper;

    private final HashMap<String, WebElementFacade> elementMap = new HashMap<>();

    @FindBy(xpath = "//h1[contains(text(),'Life Insurance')]")
    private WebElementFacade mlcHomePageLogo;

    @FindBy(xpath = "//button[@data-popover-id='global-search']")
    private WebElementFacade searchIcon;

    @FindBy(xpath = "//input[@type='search']")
    private WebElementFacade inputFieldforSearch;

    public void pageIsLoaded() {
        elementMap.put("search icon", searchIcon);
    }

        public void launchHomePage( ) {
        String url= ParameterProvider.getParameter(ParameterProvider.ParameterName.URL, ParameterProvider.AppName.MLCUI);
        getDriver().get(url);
        if (mlcHomePageLogo.isVisible()) {
            System.out.println("Launched the home page");
        } else {
            Assert.fail("Unable to launch home page");
        }

    }

    public void verifyHomePageisDisplayed() {
        if (mlcHomePageLogo.isVisible()) {
            LOGGER.info("Home page is displayed");
        } else {
            Assert.fail("Home Page is not displayed");
        }

    }

    public void clickOnOption(String optionType) {
        try {
            if(optionType.equalsIgnoreCase("Search Icon")) {
                WebDriverWait wait = new WebDriverWait(getDriver(), 10);
                wait.until(ExpectedConditions.visibilityOf(searchIcon));
            }
            elementMap.get(optionType.toLowerCase()).click();
        } catch (Exception e) {
            Assert.fail("Exception during button click " + e);
        }

    }

    public void enterSearchText(String text) {
//        inputFieldforSearch.click();
        pageHelper.sendStringAsCharArray(inputFieldforSearch,text);

    }

    public void selectFromList(String option) {
        ListOfWebElementFacades dropdownList=findAll("//div[@class='autocomplete-list']/ul/li");
        for (WebElement element : dropdownList) {
            if (element.getText().contains(option)) {
                element.click();
                break;
            }
        }
    }


}
